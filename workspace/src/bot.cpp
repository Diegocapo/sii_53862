#include "DatosMemoriaCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sys/mman.h>

int main(){
	int fd;
	DatosMemCompartida *pdatos;
	
	//Tratamiento del error al abrir el fichero
	if ((fd=open("datosbot",O_RDWR))<0)
	{
		perror("Error en la apertura del bot");
		return 1;
	}
	
	//Proyección en memoria
	pdatos = static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE | PROT_READ,MAP_SHARED, fd, 0));
	close(fd);
	
	//Control del bot
	while(1){
		if((pdatos->esfera.centro.y > pdatos->raqueta2.y2)&&(pdatos->esfera.centro.y < pdatos->raqueta2.y1))
			pdatos->accion = 0;
		else if(pdatos->esfera.centro.y < pdatos->raqueta2.y2)
			pdatos->accion = -1;
		else if(pdatos->esfera.centro.y > pdatos->raqueta2.y2)
			pdatos->accion = 1;
		usleep(25000);
	}
	//munmap(pdatos,sizeof(DatosMemCompartida));
	unlink("datosbot");
}
