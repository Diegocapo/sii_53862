#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "Puntos.h"
#include "glut.h"

int main (void)
{
    int ret; 
    int fd;
    Puntos punt;
    //Creacción de la tubería
    ret = mkfifo("fifo", 0666);
    //Apertura de esta y tratamiento del error
    fd = open ("fifo", O_RDONLY);
    if (fd < 0){
       perror ("Error en la apertura");
       return 1;
    }
    //Bucle infinito de recepción de datos
    while(1) {
         if (read (fd, &punt, sizeof(punt)) < sizeof(punt)){
           perror("Error en la lectura");
           break;
         }
         else{
           if (punt.lastgoal == 1){
             printf ("El jugador 1 marca 1 punto, lleva un total de %d puntos \n", punt.pj1);
             if (punt.pj1 == 3){
               printf("Fin de la partida. Gana el jugador 1");
               close(fd);
           unlink ("fifo");
           break;
             }
           }
           if (punt.lastgoal == 2){
             printf ("El jugador 2 marca 1 punto, lleva un total de %d puntos \n", punt.pj2);
             if (punt.pj2 == 3){
               printf("Fin de la partida. Gana el jugador 2");
               close(fd);
           unlink ("fifo");
           break;
             }
           }
         }
    }
    //Cierre de la tubería
    close(fd);
    unlink ("fifo");
    return 0;
}
