// MundoCliente.h: interface for the MundoCLiente class.
//
// Autor: Diego Caparros
//////////////////////////////////////////////////////////////////////

#pragma once

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#if _MSC_VER > 1000
#endif // _MSC_VER > 1000

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <vector>

#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemoriaCompartida.h"


class MundoCliente  
{
public:
	void Init();
	MundoCliente();
	virtual ~MundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	//Atributos del juego
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	//Atributos de la puntuación
	int puntos1;
	int puntos2;
	
	//Atributos relacionados al bot y su memoria compartida
	DatosMemCompartida datosmc;
	DatosMemCompartida *pdatosmc;
	int fdb;
	
	//Atributos relacionados con el fifo del servidor y de las teclas
	int fifoserv;
	int fdteclas;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
