// MundoServ.h: interface for the MundoServ class.
//
// Autor: Diego Caparros
//////////////////////////////////////////////////////////////////////

#pragma once

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#if _MSC_VER > 1000
#endif // _MSC_VER > 1000

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <vector>
#include <pthread.h>

#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"
#include "Puntos.h"
#include "DatosMemoriaCompartida.h"

class MundoServ  
{
public:
	void Init();
	MundoServ();
	virtual ~MundoServ();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecepcionComandos();

	//Atributos del juego
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	///Atributos relacionados al logger y la puntuación
	int puntos1;
	int puntos2;
	Puntos punt;
	int fd;
	
	//Atributos relacionados al hilo para la comunicación concurrente
	int fifoserv;
	int fdteclas;
	pthread_t hilo;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
